// Satie4Unity, audio rendering support for Unity
// Copyright (C) 2016  Zack Settel

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------
using UnityEngine;
using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using OscSimpl;


public class SATIErendererCtl : MonoBehaviour
{
	
    public bool dspState = true;
    private bool _dspState;

    [Range(-90f, 18)]
    public float outputGainDB = -10f;
    private float _outputGainDB;
    
    [Range(-24f, 24)]
    public float outputTrimDB = -0f;
    private float _outputTrimDB;
    
    public bool dim = false;
    private bool _dim;
    
    public bool mute = false;
    private bool _mute;

    //private bool _start = false;


    public string outputFormat = "unused";
    // builtin choices:  stereo quad five.one seven.one octo dome mono labodome
    private string _outputFormat;


    [Tooltip ("enables debug mode on renderer")]
    public bool rendererDebugFlag = false;
    private bool _rendererDebugFlag;


    private string _renderCtlmess = "/satie/renderer";


    private string _satieSceneAddr = "/satie/scene"; 

    private bool _initialized = false;

    private SATIEsetup SATIEsetupCS;


    private bool _start = false;



    public void UnityOSCTransmitter()
    {
    }


    public void Start()
    {
        SATIEsetupCS = transform.GetComponent<SATIEsetup>();   // look for SATIEsetup component in this transform
        
        if (!SATIEsetupCS)
        {
            Debug.LogError(transform.name + " : " + GetType() +  ".start(): SATIEsetup class component not found in transform : can't run, aborting");
            Destroy(this);
        }
            
        _initialized = true;
        _dspState = dspState;
        _outputGainDB = outputGainDB;
        _dim = dim;
        _outputTrimDB = outputTrimDB;
        _mute = mute;
        _outputFormat = outputFormat;
        _rendererDebugFlag = rendererDebugFlag;
 

        updateGainDB();
        updateTrimDB();
        updateMute();
        updateOutputFormat();
        updateSatieDebugMode();
    }
        
    private void updateDSPstate()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setDSP");
        float state = (dspState) ? 1f : 0f;
        
        message.Add(state);
        SATIEsetup.sendOSC(message);
    }
        

    public void setSatieDebugMode(float state)
    {
        
        _rendererDebugFlag = rendererDebugFlag = (state == 0) ? false : true;
        updateSatieDebugMode();
    }

    public void updateSatieDebugMode()
    {
        OscMessage message = new OscMessage(_satieSceneAddr+"/debugFlag");

        //message.Add("debugFlag");
        message.Add(rendererDebugFlag);
        SATIEsetup.sendOSC(message);
    }

    public float getOutputDB()
    {
        return outputGainDB;
    }


    public void setOutputDB(float db)
    {
        _outputGainDB = outputGainDB = db;
        updateGainDB();
    }

    private void updateGainDB()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setOutputDB");
        
        message.Add(outputGainDB);
        SATIEsetup.sendOSC(message);
    }

 
    public void setOutputTrimDB(float db)
    {
        _outputTrimDB = outputTrimDB = db;
        updateTrimDB();
    }

    private void updateTrimDB()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setOutputTrimDB");
        
        message.Add(outputTrimDB);
        SATIEsetup.sendOSC(message);
    }

    public void setOutputMute(float state)
    {
        _mute = mute = (state > 0);
        updateMute();
    }

    private void updateMute()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setOutputMute");
        float state = (mute) ? 1f : 0f;
        
        message.Add(state);
        SATIEsetup.sendOSC(message);
    }

    public void setOutputDim(float state)
    {
        _dim = dim = (state > 0);
        updateDim();
    }

	public void clearScene(float state)
	{
		if (state < 1) return;

		OscMessage message = new OscMessage(_satieSceneAddr+"/clear");
		SATIEsetup.sendOSC(message);
	}

	public void clearScene()
	{
		OscMessage message = new OscMessage(_satieSceneAddr+"/clear");
		SATIEsetup.sendOSC(message);
	}


    private void updateDim()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setOutputDim");
        float state = (dim) ? 1f : 0f;
        
        message.Add(state);
        SATIEsetup.sendOSC(message);
    }

    private void updateOutputFormat()
    {
        OscMessage message = new OscMessage(_renderCtlmess+"/setOutputFormat");
        message.Add(outputFormat);
        SATIEsetup.sendOSC(message);
    }



    // called when inspector's values are modified
    public virtual void OnValidate()
    {
//        if (!_start)
//            return;
        
        if (!_initialized)
            return;
		
        if (_rendererDebugFlag != rendererDebugFlag)
        {
            _rendererDebugFlag = rendererDebugFlag;
            updateSatieDebugMode();
        }


        if (_outputGainDB != outputGainDB)
        {
            _outputGainDB = outputGainDB;
            updateGainDB();
        }
		
        if (_outputTrimDB != outputTrimDB)
        {
            _outputTrimDB = outputTrimDB;
            updateTrimDB();
        }
		
        if (_outputFormat != outputFormat)
        {
            _outputFormat = outputFormat;
            updateOutputFormat();
        }
	
        if (_dim != dim)
        {
            _dim = dim;
            updateDim();
        }
		
        if (_mute != mute)
        {
            _mute = mute;
            updateMute();
        }
		
        if (_dspState != dspState)
        {
            _dspState = dspState;
            updateDSPstate();
        }
    }
}

