﻿// Satie4Unity, audio rendering support for Unity
// Copyright (C) 2016  Zack Settel

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------
//using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using OSC.NET;
using System.Linq;
using System.Text.RegularExpressions;



[System.Serializable]
public class satiePluginItem
{
    //public string[] pluginTypes;
    public string name;

    public satiePluginItem()
    {
//        int i = 0;
//        pluginTypes = new string[SATIEsetup.satieQueryTypeNames.Length];
//        foreach(string s in SATIEsetup.satieQueryTypeNames)
//            pluginTypes[i++] = s;
        name = "";
    }
}


[System.Serializable]
public class satieNodeProperty
{
    public string name;
    public string value;
    public bool dirty;

    public satieNodeProperty()
    {
    }

    public satieNodeProperty(satieNodeProperty prop)
    {
        name = prop.name;
        value = prop.value;
        dirty = prop.dirty;
    }

    public satieNodeProperty(string key, string val)
    {
        name = key;
        value = val;
        dirty = false;
    }
}




public class SATIEnode : MonoBehaviour
{

    public  enum satiePluginTypes
    {
        sound,
        effect,
        process
    }

    public bool debug = false;

    public bool nodeEnabled = true;
    private bool _state;

    private bool _start = false;

    [HideInInspector] 
    //   public bool isProcess = false;


 
    public string AssetPath = "unused";
    // subdirectory of Assets/StreamingAssets
    private string _assetPath = "";

    [Header("")]
    public satiePluginTypes satiePluginType = satiePluginTypes.sound;

   // [HideInInspector] 
    public string uri = "";    // this will be deprocated
    private string _uri;


    public satiePluginItem plugin = new satiePluginItem();

    //[HideInInspector]
    public List <satieNodeProperty> nodePropertyList;
    public List <satieNodeProperty> _nodePropertyList;

    public List <string> PropertyMessages = new List<string>();
    private List <string> _PropertyMessages = new List<string>();
    // used to detect changes in the Inspector

    [Header("Node Update Settings")]
	 
   
    public bool UpdateUsingChangeThresh = false;
    public float angleThresh = 5f;
    public float movementThresh = .05f;
    private float _movementThreshSqu;
 

    // used internally for node's scenegraph update status
    private Quaternion _lastSpatUpdateRotation;
    private Vector3 _lastSpatUpdatePos;
    private Vector3 _lastPos;
    private Quaternion _lastRot;
    //private float _lastUpdateTime = 0f;
    
    [HideInInspector] 
    public bool updateRotFlag = false;

    [HideInInspector] 
    public bool updatePosFlag = false;

    [HideInInspector] 
    public static List<SATIEnode> sourceInsatances = new List<SATIEnode>();

    [HideInInspector] 
    public static List<SATIEnode> listenerInsatances = new List<SATIEnode>();

    [HideInInspector] 
    public static List<SATIEnode> groupInsatances = new List<SATIEnode>();


    [HideInInspector] 
    static public int nodeCount = 0;
    //  nodes cereated since game start
    
    [HideInInspector] 
    public string nodeName = "noName";
    
    //[HideInInspector]
    //public int nodeNo;

    [HideInInspector] 
    public string nodeType = "source";



//    public void setPlugin(string pname)
//    {
//
//        if (pname == "")
//        {
//            switch (satiePluginType)
//            {
//                case satiePluginTypes.sound :
//                    pname = "default";
//                    break;
//                case SATIEnode.satiePluginTypes.effect :
//                    pname = "defaultFx";
//                    break;
//                case SATIEnode.satiePluginTypes.process :
//                    pname = "defaultProcess";
//                    break;
//            }
//        }
//
//
//
//        if (satiePluginType == satiePluginTypes.sound)   // trim off whitespaces for sound type sources
//        {
//            Regex trimmer = new Regex(@"\s\s+");
//            pname = trimmer.Replace(pname, " ");
//        }
//
//        plugin.name = pname;
//        if (debug) Debug.Log(transform.name+" "+GetType()+".setPlugin(): "+plugin.name);
//   }


    public void setProperties(List <satieNodeProperty> props)
    {

        if (debug) Debug.Log("setProperties: props count = "+ props.Count);
        nodePropertyList.Clear();
        _nodePropertyList.Clear();
 
        foreach (satieNodeProperty p in props)
        {
            nodePropertyList.Add(new satieNodeProperty(p));
        }

        foreach (satieNodeProperty p in props)
        {
            _nodePropertyList.Add(new satieNodeProperty(p));
        }


        if (debug)  foreach (satieNodeProperty p in nodePropertyList)
        {
            Debug.Log("setProperties:  key: "+ p.name+"  val: "+p.value);
        }

        if (Application.isPlaying)
            sendProperties(true);
    }

    public void deleteProperty(string name)
    {
        Debug.Log("deleteProperty: "+ name);

        List <satieNodeProperty> tempList = new List<satieNodeProperty>();

        foreach (satieNodeProperty p in nodePropertyList)
        {
            if (p.name == name) continue;

            tempList.Add( new satieNodeProperty(p));
        }
        nodePropertyList.Clear();
        _nodePropertyList.Clear();

        foreach (satieNodeProperty p in tempList)
        {
            nodePropertyList.Add( new satieNodeProperty(p));
            _nodePropertyList.Add( new satieNodeProperty(p));
        }
    }

    // called when inspector's values are modified
    public virtual void OnValidate()
    {

//        Debug.Log(transform.name + "  " + GetType() + ".OnValidate()");

//        if (_uri != uri)
//        {
//            Regex trimmer = new Regex(@"\s\s+");
//
//            uri = trimmer.Replace(uri, " ");
//            _uri = uri;
//        }



        // check properties' states
        if (_nodePropertyList.Count != nodePropertyList.Count)
        {
            _nodePropertyList.Clear();
            foreach (satieNodeProperty nodeProp in nodePropertyList)
            {

                satieNodeProperty _prop = new satieNodeProperty(nodeProp);
//                _prop.name = nodeProp.name;
//                _prop.value = nodeProp.value;
                _prop.dirty = true;
                _nodePropertyList.Add(_prop);
            }
        } else
        {
            // attempt to locate changed value among existing keys
            for (int i = 0; i < nodePropertyList.Count; i++)
            {
                bool found = false;
                foreach (satieNodeProperty p in _nodePropertyList)
                {
                    if (p.name == nodePropertyList [i].name)
                    {
                        found = true;
                        if (p.value != nodePropertyList [i].value)
                        {
                            p.value = nodePropertyList [i].value;
                            p.dirty = true;
                        }
                    }
                }
                if (! found)
                { 
                    _nodePropertyList[i].dirty = true;   // property key not found, its name must have changed
                    _nodePropertyList[i].name = nodePropertyList[i].name;
                }
            }
        }

        if (!_start)
            return;


//        foreach (satieNodeProperty p in _nodePropertyList)
//            if (p.dirty) Debug.Log(p.name+"  IS DIRTY");

        // Debug.LogError("SATIEnode.OnValidate() for source: " + nodeName);


//        if (_uri != uri)
//        {
//            Regex trimmer = new Regex(@"\s\s+");
//
//            uri = trimmer.Replace(uri, " ");
//            _uri = uri;
//        }

        if (_state != nodeEnabled)
        {
            _state = nodeEnabled;
            setNodeActive(nodeName, nodeEnabled);
        }
        sendProperties(false);  // only send Changed properties
    }

    void sendProperties(bool sendAll)
    {
        foreach (satieNodeProperty nodeProp in _nodePropertyList)
        {
            if (nodeProp.dirty || sendAll)
            {
                sendOSCprop(nodeProp.name, nodeProp.value);
                nodeProp.dirty = false;
            }
        }
    }

    public virtual  void Awake()
    {
        _start = false;

        if (uri != "" || PropertyMessages.Count > 0)   // old data still in object,  remove this as soon as uri and PropertyMessages are depricated
        {

            satieUtilities.updateNode(this);
        }

//        if (uri != "") setPlugin(uri);   // eliminate this once the URI has been depricated
//
//        if (uri == "")
//        {
//            uri = "default";
//            // Debug.LogWarning("SATEnode.Awake: node:" + nodeName + " URI string is empty, setting URI to default plugin");
//        }
//        _uri = uri;
     }
	
    public virtual  void Start()
    {
        OnValidate(); // this is called before _start so that the URI whitespace removal is carried out.

        _start = true;

        // copy property state to for change detection
//        foreach (satieNodeProperty p in nodePropertyList)
//            _nodePropertyList.Add(new satieNodeProperty(p));

        // createNode(); must have already been called from subclass "Start()" method
        _lastRot = _lastSpatUpdateRotation = (Quaternion)transform.rotation;
        _lastPos = _lastSpatUpdatePos = transform.position;
        _movementThreshSqu = movementThresh * movementThresh;

    }


    void OnEnable()
    {
        if (!_start)
            return;
        if (nodeEnabled)
            setNodeActive(nodeName, true);
    }

    void OnDisable()
    {
        if (!_start)
            return;
        if (nodeEnabled)
            setNodeActive(nodeName, false);
    }



    public void setActive(bool state)
    {
        _state = nodeEnabled = state;
        setNodeActive(nodeName, nodeEnabled);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        //Debug.Log("PAUSED");
    }

    // called from subclass "Start()" method
    public bool initNode()
    {
        bool result = false;
        string pluginString;


        if (!SATIEsetup.OSCenabled)
        {
            Debug.LogWarning(transform.name + ":  SATIEnode.Start:  SATIEsetup: translator(s) not enabled");
            //return false;
        }

        List <SATIEnode> nodeList = new List<SATIEnode>();

        //  if  "PROCESS" becomes a node type, it will need to be subclassed from source, and given a new type
        switch (nodeType)
        {
            case "listener":
                nodeList = listenerInsatances;                    
                break;
            case "source":
                nodeList = sourceInsatances;
                break;
            case "group":
                nodeList = groupInsatances;
                break;
        }
                

        foreach (SATIEnode node in nodeList)
        {
            if (transform.name == node.name)
            {
                transform.name = transform.name + "_" + transform.GetInstanceID();  
                Debug.LogWarning("SATIEnode: initNode:  duplicate node name found. Renaming node: " + transform.name);
            }
        }
        nodeList.Add((SATIEnode)this);


        //nodeNo = nodeCount++;

        if (!plugin.name.Equals(""))
        {
            if (AssetPath != "" && AssetPath != "unused")   // insert the full path in the URI message
            {
                _assetPath = Application.streamingAssetsPath + Path.DirectorySeparatorChar + AssetPath;
				
                pluginString = plugin.name.Replace("//", "//" + _assetPath + Path.DirectorySeparatorChar);
            } else
                pluginString = plugin.name;
			

        } else
            pluginString = "";
        //Debug.Log("********************************************initNode: node: "+transform.name+"   URI : " + pluginString);
            


        //Debug.Log("URI STRING: "+pluginString);

        if (nodeType == "listener")
        {
            nodeName = transform.name; // + "_" + nodeNo;

            // satie creates one single listener by default, no need to tell satie to create a listener
            //result = SATIEsetup.createListener(nodeName, pluginString);
            // sendUri(pluginString); // not used any more
        } 

		// this should be done in the superclass-- 
		else if (nodeType == "source")
        {
            SATIEsource src = (SATIEsource)this;
            string str;
  
            nodeName = transform.name;  // + "_" + nodeNo;
 
            if (satiePluginType == satiePluginTypes.process)
                result = SATIEsetup.createProcess(nodeName, plugin.name, src.group);
            else if (satiePluginType == satiePluginTypes.sound)
                result = SATIEsetup.createSource(nodeName, plugin.name, src.group);
            else if (satiePluginType == satiePluginTypes.effect)
                result = SATIEsetup.createEffect(nodeName, plugin.name, src.group);


            //sendUri(pluginString);   //NO NEED TO DO THIS NOW THAT THE URI IS CREATED WITH THE SOURCE
            //Debug.Log("******************************************SATIEnode.initNode: node "+nodeName+"  type: "+ satiePluginType);
            //Debug.Log("******************************************SATIEnode.initNode: source nodename; "+nodeName+"  groupName: "+ src.group);
        } else if (nodeType == "group")
        {
            nodeName = transform.name;  // + "_" + nodeNo;
            result = SATIEsetup.createGroup((SATIEgroup)this, satiePluginType);
        } else
        {
            Debug.LogWarning("SATIEnode.initNode: nodetype not recognized");
            return false;
        }

        _state = nodeEnabled;
        // moved to initProperties
//       setNodeActive(nodeName, nodeEnabled);
        
        //Debug.Log("SATIEnode.Update: CREATING SPAT_OSCNODE: "+nodeName);

        //sendProperties();

        SATIEsetup.SATIEnodeList.Add(this);

        transform.name = nodeName;  // overwrite node name with satie unique name



        StartCoroutine(initProperties());

        return result;
    }


    IEnumerator initProperties() // this is delayed to make sure the audio renderer has time to create the node beforehand
    {
        yield return new WaitForFixedUpdate();
        //yield return new WaitForSeconds(.05f);

        setNodeActive(nodeName, nodeEnabled);

        sendProperties(true);

    }


    // OLD STYLE - NEEDS TO BE REDONE
    public void sendEvent(string keyWord, string CommaDelimitedValuesString)
    {
        int ivalue;
        float fvalue;
        string path;

        List<string> values = new List<string>(CommaDelimitedValuesString.Split(','));
        //names.Reverse();

        List<object> items = new List<object>();

        if (satiePluginType == satiePluginTypes.process)
            path = "/satie/process/eval";
        else
            path = "/satie/" + nodeType + "/setvec";
        
 
        items.Add(nodeName);
               
        items.Add(keyWord);

        
        foreach (string svalue in values)
        {
            if (int.TryParse(svalue, out ivalue))
            {
                //Debug.Log("ITEM IS AN INTEGER = " + ivalue);
                items.Add(ivalue);
            } else if (float.TryParse(svalue, out fvalue))
            {
                //Debug.Log("ITEM IS A FLOAT = " + fvalue);
                items.Add(fvalue);
            } else
            {
                //Debug.Log("ITEM IS A STRING = " + svalue);
                items.Add(svalue);
            }
        }
 
        SATIEsetup.OSCtx(path, items);
        items.Clear();
    }

    //   /satie/source/setvec sourceName eventName <opt> atom1 atom2...atomN
    //  /satie/process/eval eventName <opt> atom1 atom2...atomN
    public void sendEvent(List<object> items)   // items contains keyword data1 data2..... dataN
    {
        string path;
        List<object> outList = new List<object>(items);

        outList.Insert(0, nodeName);

        if (satiePluginType == satiePluginTypes.process)
            path = "/satie/process/eval";
        else
            path = "/satie/" + nodeType + "/setvec";
  
		
        if (debug)
        {
            string sheefa = "";

            foreach (object o in items)
            {
                sheefa += o.ToString();
                sheefa += " ";
            }
            Debug.Log(transform.name + " : " + GetType() + " : " + "sendEvent: " + path + " " + nodeName + " " + sheefa);
        }

        SATIEsetup.OSCtx(path, outList);
        outList.Clear();
    }


    //  /satie/source/state sourceName value
    public virtual void  setNodeActive(string nodeName, bool state)
    {
        string path; 
        if (satiePluginType == satiePluginTypes.process)
            path = "/satie/process/state";
        else
            path = "/satie/" + nodeType + "/state";
        // string path = "/spatosc/core/" + nodeType + "/" + nodeName + "/state";

        List<object> items = new List<object>();

        items.Add(nodeName);

        if (state)
            items.Add(1);
        else
            items.Add(0);
              
        SATIEsetup.OSCtx(path, items);
        items.Clear();
    }


    //  /satie/scene deleteNode nodeName
    public virtual void deleteNode(string nodeName)
    {
        string path = "/satie/scene";
        List<object> items = new List<object>();

        items.Add("deleteNode");
        items.Add(nodeName);        

        SATIEsetup.OSCtx(path, items);
        items.Clear();

    }
     

    // Update is called once per frame
    public virtual void Update()
    {
        //if (Input.GetKeyDown("s")) sendProperties();
        //if (Input.GetKeyDown("a")) sendEvent("sheefa", "1,2,-3,4.1");
    }


    public virtual void FixedUpdate()
    {
		 
    }


    // lateUpdate is called onece per frame, after physics engine is updated
    public virtual void LateUpdate()
    {
        updateNode(); 
    }
	


   
    // called to set node's update flags
    void updateNode()
    {
        if (UpdateUsingChangeThresh)  // filter by amount of change since last spatOSC update
        {
            float angleDif = Quaternion.Angle(transform.rotation, _lastSpatUpdateRotation);
            float positionDif = Vector3.SqrMagnitude(transform.position - _lastSpatUpdatePos);

            if (positionDif > _movementThreshSqu)
            {
                //SATIEsetup.setPositionWrapper(nodeName,transform.position.x, transform.position.y, transform.position.z);
                updatePosFlag = true;
                _lastSpatUpdatePos = transform.position;   
                if (debug)
                    Debug.Log("SATIEnode.Update: filter mode update position for " + nodeName);
            }     

            if (angleDif > angleThresh || angleDif < -angleThresh)
            {
                Vector3 orientation = transform.rotation.eulerAngles;

                //SATIEsetup.setOrientation(nodeName, orientation.x, orientation.y, orientation.z); 
                updateRotFlag = true;
                _lastSpatUpdateRotation = (Quaternion)transform.rotation;
                if (debug)
                    Debug.Log("SATIEnode.Update: " + nodeName + " orientation = " + orientation);
            }
        } else  // Else just simple change filter   (any change at all)
        {
            if (transform.position != _lastPos)
            {
                //SATIEsetup.setPositionWrapper(nodeName,transform.position.x, transform.position.y, transform.position.z);
                updatePosFlag = true;
                _lastPos = transform.position;
                if (debug)
                    Debug.Log("SATIEnode.Update: filter mode update position for " + nodeName);
            }
            
            if (_lastRot != (Quaternion)transform.rotation)
            {
                Vector3 orientation = transform.rotation.eulerAngles;
                
                //SATIEsetup.setOrientation(nodeName, orientation.x, orientation.y, orientation.z);
                updateRotFlag = true;
                _lastRot = (Quaternion)transform.rotation;
                
                if (debug)
                    Debug.Log("SATIEnode.Update: " + nodeName + " orientation = " + orientation);
            }
        }
        //Debug.Log("spatOECnode.updateNode: time delta:" + (Time.realtimeSinceStartup - _lastUpdateTime));
        //  _lastUpdateTime = Time.realtimeSinceStartup;
    }

    void OnDestroy()
    {
        if (SATIEsetup.OSCenabled)
        {

            // SATIEnode node = (SATIEnode) this;

            SATIEsetup.SATIEnodeList.Remove(this);

            switch (nodeType)
            {
				
                case  "listener": 
                    listenerInsatances.Remove(this);
                    break;
                case  "source":
                    sourceInsatances.Remove(this);
                    break;
                case "group":
                    groupInsatances.Remove(this);
                    break;
            }

            deleteNode(nodeName);
        }
    }


    // prints a warning message and returns empty string if property is not found
    public string getStrProperty(string propName)
    {

        foreach (string s in PropertyMessages)
        {

            if (!s.StartsWith(propName))
                continue;


            List<string> items = new List<string>(s.Split(' '));
            foreach (string item in items)
            {
                if (item.Equals(' '))
                    continue;
                
                return item;
            }
        }
        Debug.LogWarning(transform.name + " " + GetType() + ":getProperty() prop: " + propName + " not able to match property, and or: bad value format, must be a string"); 
        return "";
    }

    // prints a warning message and returns zero if property is not found
    public float getFloatProperty(string propName)
    {

        foreach (string s in PropertyMessages)
        {

            if (!s.StartsWith(propName))
                continue;


            List<string> items = new List<string>(s.Split(' '));
            foreach (string item in items)
            {
                int ival;
                float fval;
                double dval;

                if (item.Equals(' '))
                    continue;

                if (int.TryParse(item, out ival))
                    return (float)ival;
                else if (float.TryParse(item, out fval))
                    return fval;
                else if (double.TryParse(item, out dval))
                    return (float)dval;
            }
        }
        Debug.LogWarning(transform.name + " " + GetType() + ":getProperty() prop: " + propName + " not able to match property, and or: bad value format, must be a float"); 
        return 0f;
    }


    public void setProperty(string propName, object value)
    {
        string valstr = value.ToString();

        string newProp = propName + " " + valstr;

        foreach (string s in PropertyMessages)
        {
            if (s.Contains(propName))
            {
                PropertyMessages.Remove(s);   // remove old string before appending new one 
                break;
            }
        }
        PropertyMessages.Add(newProp);
        sendOSCprop(propName, valstr);

        //Debug.Log("SATIEnode.setProperty: adding " + newProp);

    }


    public void refreshState()
    {
        string pluginString;

        if (!plugin.name.Equals(""))
        {
            if (AssetPath != "" && AssetPath != "unused")   // insert the full path in the URI message
            {
                _assetPath = Application.streamingAssetsPath + Path.DirectorySeparatorChar + AssetPath;
                
                pluginString = plugin.name.Replace("//", "//" + _assetPath + Path.DirectorySeparatorChar);
            } else
                pluginString = plugin.name;

            // no need to do this now
            // sendUri(pluginString);
        }
        sendProperties(true);
        updatePosFlag = updateRotFlag = true;

    }


//    void sendProperties()
//    {
//        foreach (string s in PropertyMessages)
//        {
//            if (s.Equals(""))
//                continue;
//
//            string keyword = "";
//            string svalue = "";
//                
// 
//
//            //string oscmess = "";
//                
//            // Debug.Log("rawmess:" + rawmess);
//                
//            List<string> items = new List<string>(s.Split(' '));
//
//                
//            foreach (string item in items)
//            {
//                if (!item.Equals(' '))
//                {
//                    if (keyword.Equals(""))
//                        keyword = item;
//                    else if (svalue.Equals(""))
//                        svalue = item;
//                    else
//                    {
//                        Debug.LogWarning(transform.name + ":  SATIEnode.Start: param mess: " + keyword + " ignoring values after " + svalue);
//                        break;
//                    }
//                }
//            }
//                
//            if (!keyword.Equals("") && !svalue.Equals(""))   // keyword and value good, now send the corrensponding properties messages
//            {
//                sendOSCprop(keyword, svalue);
//            } else
//            {
//                Debug.LogWarning(transform.name + ":  SATIEnode.Start: param mess: ignoring incomplete message");
//                break;
//            }
//        }
//    }


    //  /satie/scene/prop keyword value
    public void sendOSCprop(string keyword, string svalue)
    {


        // List<object> items = new List<object>();

        int ivalue;
        float fvalue = 0;
        OscMessage mess;

        string path;
        //SATIEsource src;

        if (satiePluginType == satiePluginTypes.process)
            path = "/satie/process/set";
        else
            path = "/satie/" + nodeType + "/set";

        mess = new OscMessage(path);


        mess.Add(nodeName);
        mess.Add(keyword);



        if (int.TryParse(svalue, out ivalue))
        {
            //Debug.Log("ITEM IS AN INTEGER = " + ivalue);
            mess.Add(ivalue);
        } else if (float.TryParse(svalue, out fvalue))
        {
            //Debug.Log("ITEM IS A FLOAT = " + fvalue);
            mess.Add(fvalue);
        } else
        {
            //Debug.Log("ITEM IS A STRING = " + svalue);
            mess.Add(svalue);
        }

        //Debug.Log(transform.name + " : " + GetType() + " : " + "sendOSCprop: "+path+" : "+keyword+" : "+fvalue  );

//        SATIEsetup.OSCtx(path, items);
//        items.Clear();

        SATIEsetup.sendOSC(mess);
    }


}
